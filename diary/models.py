from django.db import models

# Create your models here.
class Diary(models.Model):
    author = models.CharField(verbose_name='投稿者', max_length=20)
    title = models.CharField(verbose_name='タイトル', max_length=40)
    content = models.TextField(verbose_name='内容', blank=True, null=True)
    photo1 = models.ImageField(verbose_name='写真１', blank=True, null=True)
    photo2 = models.ImageField(verbose_name='写真２', blank=True, null=True)
    photo3 = models.ImageField(verbose_name='写真３', blank=True, null=True)
    created_at = models.DateTimeField(verbose_name='作成日時', auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name='更新日時', auto_now=True)

    class Meta:
        verbose_name_plural = 'Diary'

    def __str__(self):
        return self.title

class Comment(models.Model):
    text = models.TextField('本文')
    author = models.CharField(verbose_name='コメント者', max_length=20,default='匿名')
    target = models.ForeignKey(Diary, on_delete=models.CASCADE, verbose_name='どの記事へのコメントか')
    created_at = models.DateTimeField(verbose_name='作成日時', auto_now_add=True)

    def __str__(self):
        return self.text[:20]
