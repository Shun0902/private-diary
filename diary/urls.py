from django.urls import path
from . import views

app_name = 'diary'

urlpatterns = [
    path('', views.DiaryListView.as_view(), name='diary_list'),
    path('diary-detail/<int:pk>/', views.DiaryDetailView.as_view(), name='diary_detail'),
    path('comment/<int:pk>/', views.CommentCreateView.as_view(), name='comment_create'),
    path('diary-create/', views.DiaryCreateView.as_view(), name='diary_create'),
    path('diary-update/<int:pk>/', views.DiaryUpdateView.as_view(), name='diary_update'),
    path('diary-delete/<int:pk>/', views.DiaryDeleteView.as_view(), name='diary_delete'),
]
