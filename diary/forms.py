from .models import Diary, Comment
from django import forms

class DiaryCreateForm(forms.ModelForm):

    class Meta:
        model = Diary
        fields = ('author','title', 'content', 'photo1', 'photo2', 'photo3',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args,**kwargs)
        for field in self.fields.values():
            field.widget.attrs['class'] = 'form-control'


class CommentCreateForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('text','author')
        widgets = {
            'text':forms.Textarea(attrs={'class': 'textarea form-control'}),
        }
