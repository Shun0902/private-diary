# Generated by Django 4.0.1 on 2022-01-14 03:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Diary',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('author', models.CharField(max_length=20, verbose_name='投稿者')),
                ('title', models.CharField(max_length=40, verbose_name='タイトル')),
                ('content', models.TextField(blank=True, null=True, verbose_name='内容')),
                ('photo1', models.ImageField(blank=True, null=True, upload_to='', verbose_name='写真１')),
                ('photo2', models.ImageField(blank=True, null=True, upload_to='', verbose_name='写真２')),
                ('photo3', models.ImageField(blank=True, null=True, upload_to='', verbose_name='写真３')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='作成日時')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='更新日時')),
            ],
            options={
                'verbose_name_plural': 'Diary',
            },
        ),
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.TextField(verbose_name='本文')),
                ('author', models.CharField(default='匿名', max_length=20, verbose_name='コメント者')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='作成日時')),
                ('target', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='diary.diary', verbose_name='どの記事へのコメントか')),
            ],
        ),
    ]
