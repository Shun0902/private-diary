from django.shortcuts import get_object_or_404, redirect
from django.views import generic
from .models import Diary, Comment
from django.contrib.auth.mixins import LoginRequiredMixin
from .forms import DiaryCreateForm, CommentCreateForm
from django.urls import reverse_lazy
from django.db.models import Q

# Create your views here.
class DiaryListView(LoginRequiredMixin, generic.ListView):
    model = Diary
    ordering = ['-created_at']
    template_name = 'diary/diary_list.html'
    paginate_by = 5

    def get_queryset(self):
        q_word = self.request.GET.get('query')
        if q_word:
            diary_list = Diary.objects.filter(
                Q(author__icontains=q_word) | Q(title__icontains=q_word) | Q(content__icontains=q_word))
        else:
            diary_list = Diary.objects.all().order_by('-created_at')
        return diary_list

class DiaryDetailView(LoginRequiredMixin,generic.DetailView):
    model = Diary
    template_name = 'diary/diary_detail.html'

class CommentCreateView(LoginRequiredMixin,generic.CreateView):
    model = Comment
    template_name = 'diary/comment_create.html'
    form_class = CommentCreateForm

    def form_valid(self, form):
        diary_pk = self.kwargs['pk']
        diary = get_object_or_404(Diary,pk=diary_pk)
        comment = form.save(commit=False)
        comment.target = diary
        comment.save()
        return redirect('diary:diary_detail',pk=diary_pk)

class DiaryCreateView(LoginRequiredMixin,generic.CreateView):
    model = Diary
    template_name = 'diary/diary_create.html'
    form_class = DiaryCreateForm
    success_url = reverse_lazy('diary:diary_list')


class DiaryUpdateView(LoginRequiredMixin,generic.UpdateView):
    model = Diary
    template_name = 'diary/diary_update.html'
    form_class = DiaryCreateForm

    def get_success_url(self):
        return reverse_lazy('diary:diary_detail',kwargs={'pk':self.kwargs['pk']})

class DiaryDeleteView(LoginRequiredMixin, generic.DeleteView):
    model = Diary
    template_name = 'diary/diary_delete.html'
    success_url = reverse_lazy('diary:diary_list')
